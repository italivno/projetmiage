
public abstract class Carte implements Comparable{

	private String recto;
	private String verso;

	public Carte(String recto, String verso) {
		this.recto = recto;
		this.verso = verso;
	}

	abstract String getValue();

	public String getRecto() {
		return recto;
	}

	public String getVerso() {
		return verso;
	}
	
	@Override
	public int compareTo(Object arg0) {
		if (Double.parseDouble(this.getValue()) == Double.parseDouble(((Carte) arg0).getValue()))
			return 0;
		else if (Double.parseDouble(this.getValue()) > Double.parseDouble(((Carte) arg0).getValue()))
			return 1;
		else
			return -1;
	}
}