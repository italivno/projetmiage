import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Jeu {

	private static int numPartieCompteur = 0;
	private int numeroPartie;
	private ArrayList<Joueur> joueurs;
	private ArrayList<Carte> pioche;
	private ArrayList<Carte> plateau;
	private typeJeu leJeuVoulu;
	private Jeu_main laFrame;
	private int IDJoueurActif;
	private Carte carte_selectionnee;

	/**M�thode initialisant la partie*/
	public void init(ArrayList<Joueur> lesJoueurs) {
		this.IDJoueurActif = 0;
		this.joueurs = lesJoueurs;
		this.leJeuVoulu = choix_jeu();
		this.numeroPartie = numPartieCompteur;
		try {
			this.pioche = Reader.csvReaderToCard(this.leJeuVoulu);
			this.plateau = new ArrayList<Carte>();
			distributionCartes();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (this.leJeuVoulu == typeJeu.cardline)
			choix_valeur_cardline(false);
		ordreJoueurs();
		this.laFrame.update();
	}

	/**M�thode permettant de choisir/changer la valeur avec laquelle est jou�e une partie Cardline*/
	public ValeurCardline choix_valeur_cardline(boolean update) {
		if (this.leJeuVoulu == typeJeu.timeline) { /**Si la partie en cours est en Timeline*/
			JOptionPane.showMessageDialog(
					null,
					"Il n'est pas possible de changer le param�tre de jeu avec Timeline.",
					"Action non autoris�e", 
					JOptionPane.ERROR_MESSAGE);
			return null;
		} else { /**Sinon elle est en Cardline*/
			ValeurCardline result = (ValeurCardline)JOptionPane.showInputDialog(
			laFrame,
	        "Sur quel crit�re voulez-vous baser votre partie Cardline ?\n",
	        "Choix de la valeur",
	        JOptionPane.PLAIN_MESSAGE,
	        new ImageIcon(""),
	        ValeurCardline.values(),
	        CarteCardline.getValActive());
			if (result == null)
				result = CarteCardline.getValActive();
			CarteCardline.setValActive(result);
			Collections.sort(this.plateau);
			if (update)
				this.laFrame.update();
	        return result;
		}
	}
	
	/**M�thode permettant de d�terminer � quel jeu l'on veut jouer*/
	public typeJeu choix_jeu() {
		typeJeu result = (typeJeu)JOptionPane.showInputDialog(
		laFrame,
        "� quel jeu voulez-vous jouer ?\n",
        "Choix du jeu",
        JOptionPane.PLAIN_MESSAGE,
        new ImageIcon(""),
        typeJeu.values(),
        typeJeu.cardline);
		if (result == null)
			result = typeJeu.timeline; /**On lance par d�faut Timeline si rien n'est s�lectionn�.*/
        return result;
	}	
	
	/**M�thode constructeur frame*/
	public Jeu(Jeu_main laFrame) throws IOException {
		numPartieCompteur +=1;
		this.laFrame = laFrame;
	}
	
	/**M�thode permettant au joueur courant d'agir*/
	public void action_joueur(int la_place) {
		if (verif_placement(this.carte_selectionnee, la_place)) {
			JOptionPane.showMessageDialog(
					null,
					"Gagn� !",
					"Gagn� !", 
					JOptionPane.INFORMATION_MESSAGE);
			this.plateau.add(la_place, this.carte_selectionnee);
			this.joueurs.get(this.getIDJoueurActif()).removeCarte(this.carte_selectionnee);
		} else {
			JOptionPane.showMessageDialog(
					null,
					"Perdu ! \n c'etait : " + this.carte_selectionnee.getValue(),
					"Perdu !", 
					JOptionPane.ERROR_MESSAGE);
			this.joueurs.get(this.getIDJoueurActif()).removeCarte(this.carte_selectionnee);
			this.joueurs.get(this.getIDJoueurActif()).addCarte(retirerCartes(1));
		}
		if (this.joueurs.get(IDJoueurActif).getMain().size() == 0) {
			JOptionPane.showMessageDialog(
					null,
					"La partie est termin�e !",
					"Game over", 
					JOptionPane.INFORMATION_MESSAGE);
			this.laFrame.fermer();
		}
		this.IDJoueurActif++;
		if(this.joueurs.size()==IDJoueurActif) {
			this.IDJoueurActif = 0;
		}
		this.carte_selectionnee = null; /**R�initialisation de la carte s�lectionn�e*/ 
		this.laFrame.update(); /**Mise � jour de l'�tat de la partie.*/ 
	}
	
	/**M�thode permettant de v�rifier le placement de la carte s�lectionn�e sur la plateau de jeu*/
	private boolean verif_placement(Carte la_carte, int la_place) {
		boolean verif = false;
		if (la_place == 0) {
			verif = Double.parseDouble(this.plateau.get(0).getValue()) > Double.parseDouble(la_carte.getValue());
		} else if (this.plateau.size() == la_place) {
			verif = Double.parseDouble(this.plateau.get(this.plateau.size()-1).getValue()) < Double.parseDouble(la_carte.getValue());
		} else {
			verif = Double.parseDouble(this.plateau.get(la_place-1).getValue()) < Double.parseDouble(la_carte.getValue()) 
									 && 
									 Double.parseDouble(this.plateau.get(la_place).getValue()) > Double.parseDouble(la_carte.getValue()); 
		}
		return verif;
	}

	// ------ M�thodes relatives � l'ordre de jeu des joueurs ------ */

	/**M�thode permettant de trier les joueurs dans l'ordre croissant de leur �ge.*/
	private ArrayList<Joueur> triJoueurs(){
		ArrayList<Joueur>lesJoueursTriesParAgeCroissant = new ArrayList<Joueur>();
		lesJoueursTriesParAgeCroissant.addAll(joueurs);
		Collections.sort(lesJoueursTriesParAgeCroissant, new Comparator<Joueur>() {
			@Override
			public int compare(Joueur joueur1, Joueur joueur2) {
				return joueur1.compareTo(joueur2);
			}
		});
		return lesJoueursTriesParAgeCroissant;
	}

	/**M�thode permettant de d�terminer le 1er joueur.*/
	private Joueur plusJeunesJoueurs(ArrayList<Joueur>joueursOrdonnes){
		ArrayList<Joueur>lesJoueursPlusJeunes = new ArrayList<Joueur>();
		for (int i = 0 ; i<joueursOrdonnes.size() ; i++) { /**On parcourt la liste tri�e pour d�terminer le nombre de joueurs les plus jeunes*/
			if(joueursOrdonnes.get(0).getAge()==joueursOrdonnes.get(i).getAge()){ /**Si le joueur a le m�me �ge que le 1er de la liste*/
				lesJoueursPlusJeunes.add(joueursOrdonnes.get(i));
			}
		}
		/**S'il y a plusieurs joueurs du m�me �ge que le 1er de la liste, on effectue un tirage au sort dans notre nouvelle liste pour d�terminer le 1er joueur*/
		Random rand = new Random();
		Joueur joueurRandom = lesJoueursPlusJeunes.get(rand.nextInt(lesJoueursPlusJeunes.size()));
		return joueurRandom;
	}

	/**M�thode permettant de d�terminer l'ordre de jeu des joueurs.*/
	private void ordreJoueurs(){
		ArrayList<Joueur>joueursOrdonnes = triJoueurs();
		ArrayList<Joueur>joueursOrdonnesFinal = new ArrayList<Joueur>(); /**Liste finale d�terminant l'ordre des joueurs.*/
		if (joueursOrdonnes.get(0).getAge()==joueursOrdonnes.get(1).getAge()) { /**Si le 2e joueur de la liste tri�e a le m�me �ge que le 1er de cette liste, donc du plus jeune*/
			Joueur lePremierJoueur = plusJeunesJoueurs(joueursOrdonnes);
			for (int i = joueurs.indexOf(lePremierJoueur) ; i < joueurs.size() ; i++) /**Ajout des joueurs �partir du 1er joueur d�sign� de la liste non tri�e dans la liste finale*/ 
				joueursOrdonnesFinal.add(joueurs.get(i)); 
			for (int i = 0 ; joueurs.get(i)!=lePremierJoueur ; i++) /**Ajout des joueurs de d�but de liste non tri�e � la fin de la liste finale*/
				joueursOrdonnesFinal.add(joueurs.get(i)); 
		} else { /** Sinon le 1er joueur de la liste tri�e est le plus jeune de tous les joueurs*/
			for (int i = joueurs.indexOf(joueursOrdonnes.get(0)) ; i < joueurs.size() ; i++) /**Ajout des joueurs � partir du 1er joueur d�sign� de la liste non tri�e dans la liste finale*/ 
				joueursOrdonnesFinal.add(joueurs.get(i)); 
			for (int i = 0 ; joueurs.get(i)!=joueursOrdonnes.get(0) ; i++)/**Ajout des joueurs de d�but de liste non tri�e �la fin de la liste finale*/
				joueursOrdonnesFinal.add(joueurs.get(i)); 
		}
		this.joueurs=joueursOrdonnesFinal; /**On met � jour l'attribut joueurs pour notre Jeu*/
	}


	// ----- M�thodes relatives � la distribution des cartes ----- */

	/**M�thode permettant de m�langer les cartes de la pioche*/
	private void melangeCartes(){
		Collections.shuffle(pioche);
	}

	/**M�thode permettant de retirer de la pioche le nombre de cartes d�sir�es et renvoie un ArrayList avec les cartes retir�es*/
	private ArrayList<Carte> retirerCartes(int nbCartesDistribuees) {
		ArrayList<Carte> cartesRetirees = new ArrayList<Carte>(nbCartesDistribuees);
		for(int i = 0; i < nbCartesDistribuees; i++) {
			cartesRetirees.add(pioche.remove(0));
		}
		return cartesRetirees;
	}

	/**M�thode qui distribue les cartes dans les mains des joueurs ainsi que la 1re carte sur le plateau.*/
	private void distributionCartes() throws IOException {
		int nbCartesDistribuees = 0;
		melangeCartes(); /**On m�lange d'abord le paquet de cartes avant distribution aux joueurs*/
		if (joueurs.size() <= 3)
			nbCartesDistribuees = 6;
		if (joueurs.size() > 3 && joueurs.size() <= 5)
			nbCartesDistribuees = 5;
		if (joueurs.size() > 5)
			nbCartesDistribuees = 4;
		for (int i = 0 ; i < joueurs.size(); i++)
			joueurs.get(i).getMain().addAll(retirerCartes(nbCartesDistribuees));
		plateau.addAll(retirerCartes(1));
	}
	

	// ----- M�thodes usuelles (toString, getters et setters) ----- */

	public int getNumeroPartie() {
		return numeroPartie;
	}

	public ArrayList<Carte> getPlateau() {
		return plateau;
	}

	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}

	public ArrayList<Carte> getPioche() {
		return pioche;
	}
	
	public int getIDJoueurActif() {
		return IDJoueurActif;
	}
	
	public Carte getCarte_selectionnee() {
		return carte_selectionnee;
	}

	public void setCarte_selectionnee(Carte carte_selectionnee) {
		this.carte_selectionnee = carte_selectionnee;
	}
	
}