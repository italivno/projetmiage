import java.util.ArrayList;

public class Joueur implements Comparable<Joueur>{

	private String pseudo;
	private int age;
	private ArrayList<Carte> main;

	public Joueur(String pseudo, int age) {
		this.main = new ArrayList<Carte>();
		this.pseudo = pseudo;
		this.age = age;
	}

// ----- M�thodes usuelles (toString, getters et setters) ----- */

	public String getPseudo() {
		return pseudo;
	}

	public int getAge() {
		return age;
	}

	public ArrayList<Carte> getMain() {
		return main;
	}

	public void setMain(ArrayList<Carte> main) {
		this.main = main;
	}

	@Override
	public String toString() {
		return "["+pseudo+"|"+age+" ans]";
	}

	@Override
	public int compareTo(Joueur j) {
		if (this.age == j.age)
			return 0;
		else if (this.age < j.age) 
			return -1;
		else
			return 1;
	}

	public void removeCarte(Carte la_carte) {
		this.main.remove(la_carte);
	}

	public void addCarte(ArrayList<Carte> retirerCarte) {
		this.main.add(retirerCarte.get(0));
	}

// ----------------------------------------------------------- */

}