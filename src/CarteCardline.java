import java.io.IOException;

public class CarteCardline extends Carte {

	private String intitule;
	private String ville;
	private String superficie;
	private String population;
	private String pib;
	private String pollution;
	public static ValeurCardline getValActive() {
		return valActive;
	}


	public static void setValActive(ValeurCardline valActive) {
		CarteCardline.valActive = valActive;
	}

	public static ValeurCardline valActive;

	/**constructeur d'une carte pour le jeu Timeline*/
	public CarteCardline(String intitule, String ville, String superficie, String population, String pib, String pollution) throws IOException {
		super(".\\data\\cardline\\cards\\"+intitule+".jpeg", ".\\data\\cardline\\cards\\"+intitule+"_reponse.jpeg");
		this.intitule = intitule;
		this.ville=ville;
		this.superficie=superficie;
		this.population=population;
		this.pib=pib;
		this.pollution=pollution;
		}

	
// ----- Getters pour une carte du jeu Cardline ----- */
	
	public String getVille() {
		return ville;
	}

	public String getValue() {
		switch(CarteCardline.valActive) {
		case superficie:
			return this.superficie;
		case population:
			return this.population;
		case pib:
			return this.pib;
		case pollution:
			return this.pollution;
		default:
			return null;
		}
	}
}
