import java.io.IOException;

public class CarteTimeline extends Carte {

	private String invention;
	private String date;
	private String intitule;

	/**constructeur d'une carte pour le jeu Timeline*/
	public CarteTimeline(String intitule, String invention, String date) throws IOException {
		super(".\\data\\timeline\\cards\\"+intitule+".jpeg",".\\data\\timeline\\cards\\"+intitule+"_date.jpeg");
		this.intitule = intitule;
		this.date=date;
		this.invention=invention;
	}

// ----- Getter pour une carte du jeu Timeline ----- */

	public String getValue() {
		return this.date;
	}
	
// -------------------------------------------------- */


}
