import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Reader {

	/**Affecte les donn�es d'une carte dans des variables ad�quates pour le jeu choisi.*/
	public static ArrayList<Carte> csvReaderToCard(typeJeu leJeuVoulu) throws IOException {
		ArrayList<Carte> paquetDuJeu = new ArrayList<Carte>();
		String ligne = "";
		BufferedReader reader;
		if (leJeuVoulu == typeJeu.timeline) { /**Si le jeu choisi est Timeline*/
			reader = new BufferedReader (new FileReader(".\\data\\timeline\\timeline.csv")); /**Chemin d'acc�s pour le fichier CSV relatif � Timeline.*/
			reader.readLine(); /**Permet de "passer" la 1re ligne du fichier CSV qui correspond aux noms des colonnes.*/
			while((ligne=reader.readLine())!=null){
				String[]informationsCarte = ligne.split(";");
				Carte card = new CarteTimeline(informationsCarte[2],informationsCarte[0],informationsCarte[1]);
				paquetDuJeu.add(card);
			}
		} else { /**Sinon le jeu choisi est Cardline*/
			reader = new BufferedReader (new FileReader(".\\data\\cardline\\cardline.csv")); /**Chemin d'acc�s pour le fichier CSV relatif � Cardline.*/
			reader.readLine(); /**Permet de "passer" la 1re ligne du fichier CSV qui correspond aux noms des colonnes.*/
			while((ligne=reader.readLine())!=null){
				String[]informationsCarte = ligne.split(";");
				Carte card = new CarteCardline(informationsCarte[5], informationsCarte[0], informationsCarte[1].replaceAll(",","."), informationsCarte[2], informationsCarte[3], informationsCarte[4].replaceAll(",","."));
				paquetDuJeu.add(card);
			}
		}
		reader.close();
		return paquetDuJeu;
	}

}