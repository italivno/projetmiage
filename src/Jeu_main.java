import java.awt.EventQueue;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

public class Jeu_main extends JFrame {

	private JPanel contentPane;

	Jeu leJeu;

	/**M�thode de lancement du programme*/
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Jeu_main frame = new Jeu_main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**Constructeur d'une fen�tre Jeu*/
	public Jeu_main() throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		setBounds(0, 0, 1366, 740);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);


		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Jeu");
		JMenuItem menu_1 = new JMenuItem("Quitter");
		menu_1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Jeu_main.this.dispose();
			}
		});
		menu.add(menu_1);
		JMenuItem valeur_cardline = new JMenuItem("Changer la valeur de jeu de Cardline");
		valeur_cardline.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				leJeu.choix_valeur_cardline(true);
			}
		});
		menu.add(valeur_cardline);
		menuBar.add(menu);
		setJMenuBar(menuBar);

		this.leJeu = new Jeu(this);

		this.leJeu.init(creation_joueurs());
	}

	/**M�thode permettant de cr�er notre liste de joueurs*/
	public ArrayList<Joueur> creation_joueurs() throws IOException {
		Object[] liste_nbr = {2,3,4,5,6,7,8};
		int result = (int)JOptionPane.showInputDialog(
				this,
				"Bienvenue, combien y a-t-il de joueurs ?",
				"Nombre de joueurs",
				JOptionPane.PLAIN_MESSAGE,
				new ImageIcon(""),
				liste_nbr,
				2);
		ArrayList<Joueur> liste_joueur = new ArrayList<Joueur>();
		for(int i = 1; i<=result; i++) {
			String nomjoueur = JOptionPane.showInputDialog(
					this, 
					"Nom du joueur n� "+i);
			String agejoueur = JOptionPane.showInputDialog(
					this, 
					"�ge du joueur n� "+i);
			Joueur joueur = new Joueur(nomjoueur,Integer.parseInt(agejoueur));
			liste_joueur.add(joueur);

		}
		return liste_joueur;
	}

	/**M�thode permettant de mettre � jour l'�tat de la partie, visuellement*/
	public void update() {
		
		/**On vide notre Pane du dessous de l'�cran, puis on en cr�e un nouveau avec les nouvelles informations, donc celles du joueur courant*/
		contentPane = null;
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		/**On demande au joueur courant de jouer*/
		JLabel tourjoueur = new JLabel("� vous de jouer, "+leJeu.getJoueurs().get(this.leJeu.getIDJoueurActif()).getPseudo()+". Veuillez choisir une carte � d�poser sur le plateau.");
		tourjoueur.setBounds(200,30,600,18);
		contentPane.add(tourjoueur);
		
		JPanel zone_plateau = new JPanel();
		zone_plateau.setBounds(0, 0, 1066, 290);
		JButton button = new JButton("+");
		button.setBounds(0, 87, 50, 50);
		button.addActionListener(new ActionListener_carte(this, 0));
		zone_plateau.add(button);

		for (int i=0;i<this.leJeu.getPlateau().size();i++) {
			ImageIcon icon = new ImageIcon(new ImageIcon(this.leJeu.getPlateau().get(i).getVerso()).getImage().getScaledInstance(175, 262, Image.SCALE_DEFAULT));
			JLabel img = new JLabel(icon);
			img.setBounds(i*175+10, 0, 175, 262);
			zone_plateau.add(img);
			JButton button2 = new JButton("+");
			button2.setBounds(i*175+10, 87, 50, 50);
			button2.addActionListener(new ActionListener_carte(this, i+1));
			zone_plateau.add(button2);
		}

		JScrollPane scrollPane_plateau = new JScrollPane();
		scrollPane_plateau.setViewportView(zone_plateau);
		scrollPane_plateau.setBounds(150, 100, 1066, 290);
		contentPane.add(scrollPane_plateau);

		JPanel zone_main_joueur = new JPanel();
		zone_main_joueur.setBounds(0, 0, 1066, 290);

		for (int i=0;i<this.leJeu.getJoueurs().get(this.leJeu.getIDJoueurActif()).getMain().size();i++) {
			ImageIcon icon2 = new ImageIcon(new ImageIcon(this.leJeu.getJoueurs().get(this.leJeu.getIDJoueurActif()).getMain().get(i).getRecto()).getImage().getScaledInstance(175, 262, Image.SCALE_DEFAULT));
			JLabel img = new JLabel(icon2);
			img.addMouseListener(new MouseAdapterCarte(this.leJeu.getJoueurs().get(this.leJeu.getIDJoueurActif()).getMain().get(i),this));
			img.setBounds(i*175+10, 0, 175, 262);
			zone_main_joueur.add(img);
		}

		JScrollPane scrollPane_main_joueur = new JScrollPane();
		scrollPane_main_joueur.setViewportView(zone_main_joueur);
		scrollPane_main_joueur.setBounds(150, 390, 1066, 290);
		contentPane.add(scrollPane_main_joueur);
		setVisible(true);
	}

	public void fermer() {
		dispose();
	}
	
	public Jeu getLeJeu() {
		return leJeu;
	}

}
