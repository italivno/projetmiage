import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class ActionListener_carte implements ActionListener{
	
	private Jeu_main frame;
	private int num_bouton;

	public ActionListener_carte(Jeu_main frame, int num_bouton) {
		super();
		this.frame = frame;
		this.num_bouton = num_bouton;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.frame.getLeJeu().getCarte_selectionnee() != null) {
			this.frame.getLeJeu().action_joueur(this.num_bouton);
		}
		else {
			JOptionPane.showMessageDialog(null,
					"Vous devez s�lectionner une carte !", 
					"Probl�me de s�lection", 
					JOptionPane.ERROR_MESSAGE);
		}
		
	}

}
