import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MouseAdapterCarte extends MouseAdapter {
	private Carte carte;
	private Jeu_main frame;
	
	public MouseAdapterCarte(Carte carte, Jeu_main frame) {
		super();
		this.carte = carte;
		this.frame = frame;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		this.frame.getLeJeu().setCarte_selectionnee(this.carte);	
	}
}
